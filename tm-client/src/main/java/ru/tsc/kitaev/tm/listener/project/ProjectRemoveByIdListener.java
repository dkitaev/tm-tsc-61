package ru.tsc.kitaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id...";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter id");
        @NotNull final String id = TerminalUtil.nextLine();
        projectEndpoint.removeProjectById(sessionService.getSession(), id);
        projectTaskEndpoint.removeById(sessionService.getSession(), id);
        System.out.println("[OK]");
    }

}
