package ru.tsc.kitaev.tm.exception.empty;

import ru.tsc.kitaev.tm.exception.AbstractException;

public final class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error. UserId is empty.");
    }

}
