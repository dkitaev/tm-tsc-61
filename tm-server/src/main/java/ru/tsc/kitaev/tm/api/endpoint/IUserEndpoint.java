package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    UserDTO findById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    );

    @WebMethod
    void setPassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    );

    @WebMethod
    void updateUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "firstName", partName = "firstName") @NotNull final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull final String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull final String middleName
    );

}
