package ru.tsc.kitaev.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.api.service.model.ITaskService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.repository.model.TaskRepository;
import ru.tsc.kitaev.tm.repository.model.UserRepository;

import java.util.Date;

@Service
@AllArgsConstructor
public final class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    @NotNull
    @Autowired
    public TaskRepository taskRepository;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        @Nullable final User user = userRepository.findById(userId).orElse(null);
        if (user == null) throw new EntityNotFoundException();
        task.setUser(user);
        taskRepository.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @Nullable final User user = userRepository.findById(userId).orElse(null);
        if (user == null) throw new EntityNotFoundException();
        task.setUser(user);
        taskRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @NotNull final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Task task = findByIndex(userId, index);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = findByName(userId, name);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        taskRepository.save(task);
    }


    @Override
    @Transactional
    public void finishByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Task task = findByIndex(userId, index);
        task.setStatus(Status.COMPLETED);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = findByName(userId, name);
        task.setStatus(Status.COMPLETED);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        task.setStartDate(new Date());
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = findByIndex(userId, index);
        task.setStatus(status);
        task.setStartDate(new Date());
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = findByName(userId, name);
        task.setStatus(status);
        task.setStartDate(new Date());
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        taskRepository.save(task);
    }

}
